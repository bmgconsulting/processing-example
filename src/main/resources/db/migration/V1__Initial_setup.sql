
CREATE TABLE Users (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL
);

CREATE TABLE Applications (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL
);

CREATE TABLE Users_Events (
  user_id INT NOT NULL,
  event_type VARCHAR(50) NOT NULL,
  timestamp TIMESTAMP NOT NULL,
  app_id int,
  FOREIGN KEY (user_id)
    REFERENCES Users(id),
  FOREIGN KEY (app_id)
    REFERENCES Applications(id)
);

/*!-- Some test data */

INSERT INTO Users (id, name) VALUES (1, 'User1');
INSERT INTO Users (id, name) VALUES (2, 'User2');
INSERT INTO Users (id, name) VALUES (3, 'User3');
INSERT INTO Users (id, name) VALUES (4, 'User4');
INSERT INTO Users (id, name) VALUES (5, 'User5');
INSERT INTO Users (id, name) VALUES (6, 'User6');
INSERT INTO Users (id, name) VALUES (7, 'User7');
INSERT INTO Users (id, name) VALUES (8, 'User8');
INSERT INTO Users (id, name) VALUES (9, 'User9');
INSERT INTO Users (id, name) VALUES (10, 'User10');

INSERT INTO Applications (id, name) VALUES (1, 'Application1');
INSERT INTO Applications (id, name) VALUES (2, 'Application2');
INSERT INTO Applications (id, name) VALUES (3, 'Application3');
INSERT INTO Applications (id, name) VALUES (4, 'Application4');
INSERT INTO Applications (id, name) VALUES (5, 'Application5');
INSERT INTO Applications (id, name) VALUES (6, 'Application6');
INSERT INTO Applications (id, name) VALUES (7, 'Application7');
INSERT INTO Applications (id, name) VALUES (8, 'Application8');
INSERT INTO Applications (id, name) VALUES (9, 'Application9');
INSERT INTO Applications (id, name) VALUES (10, 'Application10');

INSERT INTO Users_Events (user_id, event_type, timestamp, app_id) VALUES (1, 'install', NOW(), 1);
INSERT INTO Users_Events (user_id, event_type, timestamp, app_id) VALUES (1, 'install', NOW(), 2);
INSERT INTO Users_Events (user_id, event_type, timestamp, app_id) VALUES (1, 'install', NOW(), 3);
INSERT INTO Users_Events (user_id, event_type, timestamp, app_id) VALUES (1, 'install', NOW(), 4);

