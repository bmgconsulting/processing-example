package com.example.guice.math.processors;

/**
 * A summary processor which calculates the average of the data provided
 */
public class AverageProcessor implements SummaryProcessor {

    @Override
    public double summarise(double[] data) {

        final SumProcessor sumProcessor = new SumProcessor();
        final double sum = sumProcessor.summarise(data);

        return sum / data.length;
    }
}
