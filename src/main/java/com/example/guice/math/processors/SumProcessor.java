package com.example.guice.math.processors;

/**
 * A summary processor which calculates the sum of the data provided
 */
public class SumProcessor implements SummaryProcessor {

    @Override
    public double summarise(double[] data) {

        double sum = 0;
        for (double datum : data) {
            sum += datum;
        }

        return sum;
    }
}
