package com.example.guice.math.processors;

/**
 * A summary processor which calculates the std deviation of the data provided
 */
public class StdDeviationProcessor implements SummaryProcessor {

    @Override
    public double summarise(double[] data) {

        // Calculate the variance
        final VarianceProcessor varianceProcessor = new VarianceProcessor();
        final double variance = varianceProcessor.summarise(data);

        // Return the square root of the variance
        return Math.sqrt(variance);
    }
}
