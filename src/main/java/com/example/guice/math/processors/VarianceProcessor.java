package com.example.guice.math.processors;

/**
 * A summary processor which calculates the variance of the data provided
 */
public class VarianceProcessor implements SummaryProcessor {

    @Override
    public double summarise(double[] data) {

        // calculate the average first
        final AverageProcessor averageProcessor = new AverageProcessor();
        final double average = averageProcessor.summarise(data);

        // calculate the squared differences
        final double[] diffs = new double[data.length];

        for (int i = 0; i < data.length; i++) {
            final double datum = data[i];
            diffs[i] = Math.pow(datum - average, 2);
        }

        // return the average of the squared differences
        return averageProcessor.summarise(diffs);
    }
}
