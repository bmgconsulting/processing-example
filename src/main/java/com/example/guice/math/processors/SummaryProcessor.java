package com.example.guice.math.processors;

/**
 * A simple interface for a component which takes a series of numbers
 * and performs some mathematical summary of them, possibly as a sum, an average, variance
 * and so on.
 */
public interface SummaryProcessor {

    /**
     * A summary function which summarises the provided data
     * @param data The data to summarise
     * @return A summary value of some form
     */
    double summarise(double[] data);

}
