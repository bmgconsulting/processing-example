package com.example.guice.math;


import com.example.guice.math.processors.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class Calculator {

    private final SummaryProcessor sumProcessor;
    private final SummaryProcessor averageProcessor;
    private final SummaryProcessor varianceProcessor;
    private final SummaryProcessor stdDeviationProcessor;

    @Inject
    public Calculator(SummaryProcessor sumProcessor,    // we get the default instance which is sum
                      @Named("Average") SummaryProcessor averageProcessor,  // we name the rest
                      @Named("Variance") SummaryProcessor varianceProcessor,
                      @Named("StdDeviation") SummaryProcessor stdDeviationProcessor){
        this.sumProcessor = sumProcessor;
        this.averageProcessor = averageProcessor;
        this.varianceProcessor = varianceProcessor;
        this.stdDeviationProcessor = stdDeviationProcessor;
    }

    public double sum(double[] data){
        return sumProcessor.summarise(data);
    }

    public double average(double[] data){
        return averageProcessor.summarise(data);
    }

    public double variance(double[] data){
        return varianceProcessor.summarise(data);
    }

    public double stdDeviation(double[] data){
        return stdDeviationProcessor.summarise(data);
    }

}
