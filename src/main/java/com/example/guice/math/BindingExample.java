package com.example.guice.math;


import com.example.guice.math.processors.AverageProcessor;
import com.example.guice.math.processors.StdDeviationProcessor;
import com.example.guice.math.processors.SummaryProcessor;
import com.example.guice.math.processors.VarianceProcessor;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BindingExample {

    private static final Logger LOG = LoggerFactory.getLogger(BindingExample.class);

    /**
     * The SuppressWarnings exception is to stop the compile complaining about the variables I'm not using in the
     * cast example section
     *
     * @param args command line arguments provided when running the app
     */
    @SuppressWarnings("UnnecessaryLocalVariable")
    public static void main(String[] args) {

        // Lets create some test data
        final double[] data = new double[]{1, 2, 3, 4, 5};

        // Create the injector
        final Injector injector = Guice.createInjector(new BindingModule());

        /**
         * In BindingModule we specified in the configure method that by default when we ask for an instance of
         * the SummaryProcess interface, which is simply a scaffold describing what a processor looks like, the
         * injector should return an instance of the SumProcessor class which implements the SummaryProcessor
         * interface.
         */
        final SummaryProcessor defaultProcessor = injector.getInstance(SummaryProcessor.class);
        final double summary = defaultProcessor.summarise(data);
        LOG.info("Summary = {}", summary);

        /**
         * We can also ask for a specific implementation of the SummaryProcessor interface if we want. This is possible
         * because the implementations we have require no dependencies. They have no constructors specified and so can
         * be safely created.
         */

        final AverageProcessor averageProcessor = injector.getInstance(AverageProcessor.class);
        final double average = averageProcessor.summarise(data);
        LOG.info("Average = {}", average);

        final VarianceProcessor varianceProcessor = injector.getInstance(VarianceProcessor.class);
        final double variance = varianceProcessor.summarise(data);
        LOG.info("Variance = {}", variance);

        final StdDeviationProcessor stdDeviationProcessor = injector.getInstance(StdDeviationProcessor.class);
        final double stdDeviation = stdDeviationProcessor.summarise(data);
        LOG.info("Std. Deviation = {}", stdDeviation);

        /**
         * Here we will use the Named annotation to target the instances we want. Here we need to construct the type key
         * before we can ask the injector for the instance. When using constructor injection you just specify the Named
         * annotation in front of the constructor parameter you want injected.
         */

        final Key<SummaryProcessor> sumKey = Key.get(SummaryProcessor.class, Names.named("Sum"));
        final SummaryProcessor sumProcessor2 = injector.getInstance(sumKey);
        final double sum2 = sumProcessor2.summarise(data);
        LOG.info("Sum 2 = {}", sum2);

        final SummaryProcessor averageProcessor2 = injector.getInstance(sumKey);
        final double average2 = averageProcessor2.summarise(data);
        LOG.info("Average 2 = {}", average2);

        final Key<SummaryProcessor> varianceKey = Key.get(SummaryProcessor.class, Names.named("Variance"));
        final SummaryProcessor varianceProcessor2 = injector.getInstance(varianceKey);
        final double variance2 = varianceProcessor2.summarise(data);
        LOG.info("Variance 2 = {}", variance2);

        final Key<SummaryProcessor> stdDeviationKey = Key.get(SummaryProcessor.class, Names.named("StdDeviation"));
        final SummaryProcessor stdDeviationProcessor2 = injector.getInstance(stdDeviationKey);
        final double stdDeviation2 = stdDeviationProcessor2.summarise(data);
        LOG.info("Std. Deviation 2 = {}", stdDeviation2);

        /**
         * Let's do something similar except this time using constructor injection and specify the names with annotations
         * in the constructor of the class we want an instance of
         */

        final Calculator calculator = injector.getInstance(Calculator.class);
        LOG.info("Calculator sum = {}", calculator.sum(data));
        LOG.info("Calculator average = {}", calculator.average(data));
        LOG.info("Calculator variance = {}", calculator.variance(data));
        LOG.info("Calculator std deviation = {}", calculator.stdDeviation(data));

        /**
         * A more general concept to be aware of is that you can refer to an instance of a class by it's parent type
         * or any of the interfaces it implements. So we can ask for an AverageProcessor for example but keep the reference
         * to it as a SummaryProcessor. We will not see the other methods it may implement, we will only see it through the 'lens'
         * of the SummaryProcessor interface. If we wish to refer to it as an AverageProcessor again we need to do what is called
         * a 'Type Cast', and in this instance specifically it's called a down cast. You need to be careful with these as they can
         * cause exceptions if you try to cast something into a type which it doesn't implement, bit like trying to force
         * a square peg through a round hole, it's simply the wrong 'shape'
         */

        final AverageProcessor anotherAverageProcessor = injector.getInstance(AverageProcessor.class);
        final SummaryProcessor summaryReference = anotherAverageProcessor;  // this is perfectly fine

        final AverageProcessor downCast = (AverageProcessor) summaryReference;  // this is ok in this instance although the compiler will warn that it's unsafe

        final VarianceProcessor badCast = (VarianceProcessor) summaryReference; // this will throw a ClassCastException

    }

}
