package com.example.guice.math;

import com.example.guice.math.processors.*;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class BindingModule extends AbstractModule {

    @Override
    protected void configure() {

        /**
         * We are telling Guice that if the person requesting a SummaryProcessor isn't
         * specific about the type then the default implementation to return is a SumProcessor
         */
        bind(SummaryProcessor.class).to(SumProcessor.class);

        /**
         * In this section we are assigning usages of the Named annotation to specific implementations
         */

        bind(SummaryProcessor.class).annotatedWith(Names.named("Sum")).to(SumProcessor.class);
        bind(SummaryProcessor.class).annotatedWith(Names.named("Average")).to(AverageProcessor.class);
        bind(SummaryProcessor.class).annotatedWith(Names.named("Variance")).to(VarianceProcessor.class);
        bind(SummaryProcessor.class).annotatedWith(Names.named("StdDeviation")).to(StdDeviationProcessor.class);

    }

}
