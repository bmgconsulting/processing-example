package com.example.processing;

import com.google.inject.Guice;
import com.google.inject.Injector;

import java.sql.SQLException;

public class Boot {

    public static void main(String[] args) throws SQLException {

        // Use Guice for dependency injection. Initialise the injector
        final Injector injector = Guice.createInjector(new ConfigModule(), new DatabaseModule());

        // Get an instance of the Processor and call process
        final SimpleProcessor processor = injector.getInstance(SimpleProcessor.class);
        processor.process();

    }

}
