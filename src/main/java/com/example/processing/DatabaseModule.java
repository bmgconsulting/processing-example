package com.example.processing;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;

import javax.inject.Named;
import javax.sql.DataSource;

public class DatabaseModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides @Named("db")
    Config provideConfig(@Named("app") Config config){
        return config.getConfig("db");
    }

    @Provides
    DataSource provideDataSource(@Named("db") Config config){

        final HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(config.getString("url"));
        ds.setUsername(config.getString("username"));
        ds.setPassword(config.getString("password"));

        return ds;
    }

}
