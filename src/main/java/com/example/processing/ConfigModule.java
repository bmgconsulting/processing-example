package com.example.processing;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import javax.inject.Named;

public class ConfigModule extends AbstractModule {

    private String env = null;

    @Override
    protected void configure() {
        this.env = System.getProperty("ENV", "local");
    }

    @Provides @Named("app")
    Config provideConfig(){
        return ConfigFactory.load(env + ".conf");
    }

}
