package com.example.processing;


import com.example.processing.jooq.Tables;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class SimpleProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleProcessor.class);

    private final DataSource dataSource;

    @Inject
    public SimpleProcessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void process() throws SQLException {

        // Get a connection from the pool. Since Connection is AutoCloseable we can use the try with resources
        // pattern. If our processing code throws an exception it will automatically call close on the connection
        // and return it to the pool.

        try(Connection connection = dataSource.getConnection()){

            // Read all the records from the Users_Events table using a cursor
            // If there are a lot of records a cursor will read them in batches so you don't blow the memory
            // of the processor. You could get fancier here with Streams if you really wanted to but this is more than
            // good enough

            final DSLContext fetch = DSL.using(connection, SQLDialect.MYSQL);
            final Cursor<Record> cursor = fetch.select().from(Tables.USERS_EVENTS).fetchLazy();

            // For reach record log the entry
            for (Record record : cursor) {
                LOG.info("\n{}", record);
            }
        }

    }
}
